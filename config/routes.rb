Rails.application.routes.draw do
  get 'welcome/index'

  resources :users
  resources :drawings

  root'users#index'
end
