class DrawingsController < ApplicationController
 http_basic_authenticate_with name: "admin", password: "secret"
  def matchUser(user, matches)
      skips = Array.new
      while(user.match.empty? and not matches.empty?)
        match = matches.pop
        if(user.name != match[:name] and user.name != match[:partner])
          user.match = match[:name]
        else
          skips.append(match)
        end
      end
      skips.each {|skip| matches.push(skip) } 
      user.save
  end

  def index
    users = User.all
    matches = Array.new
    users.each {|user|
      user.match = ""
      matches.push({name: user.name, partner: user.partner})
    }
    matches = matches.shuffle
    users.each {|user|
      if user.partner.present?
        matchUser(user, matches)
      end
    }
    users.each {|user|
      if user.partner.empty?
        matchUser(user, matches)
      end
    }
    @drawings = users
  end
end
